import React from "react";
import "./ui.css";
import "antd/dist/antd.css";
import { Row, Col, Button, Table } from "antd";

function UI() {
    const image = "https://picsum.photos/30/40";
    return (
      <div style={{ padding: 20 }}>
        <Row gutter={24} type='flex'>
          <Col xl={4} xxl={4} span={24}>
            <div
              style={{
                backgroundColor: "#fff",
                width: "100%",
                textAlign: "left",
              }}
            >
              <h1 className="title">Thông tin đơn hàng</h1>
              <Row>
                <Col lg={3} xl={24} xxl={24}>
                  <div className="info info1">
                    <div className="title-info">Trạng thái</div>
                    <div className="content-info content-info1">Đơn mới</div>
                  </div>
                </Col>
                <Col lg={3} xl={24} xxl={24}>
                  <div className="info">
                    <div className="title-info">Mã đơn hàng</div>
                    <div className="content-info">AD2231</div>
                  </div>
                </Col>
                <Col lg={3} xl={24} xxl={24}>
                  <div className="info">
                    <div className="title-info">Ngày tạo đơn</div>
                    <div className="content-info">20/07/2020</div>
                  </div>
                </Col>
                <Col lg={3} xl={24} xxl={24}>
                  <div className="info">
                    <div className="title-info">Đại lý đặt hàng</div>
                    <div className="content-info">Nga Hạ</div>
                  </div>
                </Col>
                <Col lg={3} xl={24} xxl={24}>
                  <div className="info">
                    <div className="title-info">Tổng thành tiền</div>
                    <div className="content-info">
                      10.000.000<span> đ</span>
                    </div>
                  </div>
                </Col>
                <Col lg={3} xl={24} xxl={24}>
                  <div className="info">
                    <div className="title-info">Số điện thoại</div>
                    <div className="content-info">0987654321</div>
                  </div>
                </Col>
                <Col lg={3} xl={24} xxl={24}>
                  <div className="info">
                    <div className="title-info">Kho giao hàng</div>
                    <div className="content-info">Chờ xác nhận</div>
                  </div>
                </Col>
                <Col lg={3} xl={24} xxl={24}>
                  <div className="info">
                    <div className="title-info">Tổng trọng lượng</div>
                    <div className="content-info">
                      10<span> Kg</span>
                    </div>
                  </div>
                </Col>
                <Col lg={3} xl={24} xxl={24}>
                  <div className="info">
                    <div className="title-info">Hình thức thanh toán </div>
                    <div className="content-info">Chưa xác nhận</div>
                  </div>
                </Col>
              </Row>
              <div style={{ display: "flex", justifyContent: "center" }}>
                <Button className="button">Xem danh sách đơn S2</Button>
              </div>
            </div>
          </Col>
          <Col xl={20} xxl={20} span={24}>
            <div
              style={{
                width: "100%",
                height: '100%',
                backgroundColor: "#fff",
                paddingRight: 25,
                paddingLeft: 25,
              }}
            >
              <Row gutter={24}>
                <Col xl={17} xxl={18} span={24}>
                  <div>
                    <h1 className="title-table">Sản phẩm đặt mua</h1>
                    <table className="table">
                      <thead>
                        <tr>
                          <th>Tên sản phẩm</th>
                          <th>Quy cách 1</th>
                          <th>Quy cách 2</th>
                          <th>Trọng lượng</th>
                          <th>Đơn giá</th>
                          <th>Thành tiên</th>
                          <th>Giảm giá</th>
                          <th>Thanh toán</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>
                            <tr style={{ border: "none" }}>
                              <td style={{ border: "none", padding: 0 }}>
                                <div
                                  style={{
                                    borderWidth: 1,
                                    borderColor: "#999",
                                    borderStyle: "solid",
                                    padding: 5,
                                    marginRight: 5,
                                  }}
                                >
                                  <img src={image} />
                                </div>
                              </td>
                              <td
                                style={{
                                  border: "none",
                                  textAlign: "left",
                                }}
                              >
                                Phân bón
                              </td>
                            </tr>
                          </td>
                          <td>10</td>
                          <td>5</td>
                          <td>100 Kg</td>
                          <td>121,000 đ</td>
                          <td>1,121,000 đ</td>
                          <td>200,000 đ</td>
                          <td>12,100,000 đ</td>
                        </tr>
                        <tr>
                          <td>
                            <tr style={{ border: "none" }}>
                              <td style={{ border: "none", padding: 0 }}>
                                <div
                                  style={{
                                    borderWidth: 1,
                                    borderColor: "#999",
                                    borderStyle: "solid",
                                    padding: 5,
                                    marginRight: 5,
                                  }}
                                >
                                  <img src={image} />
                                </div>
                              </td>
                              <td
                                style={{
                                  border: "none",
                                  textAlign: "left",
                                }}
                              >
                                Phân bón
                              </td>
                            </tr>
                          </td>
                          <td>10</td>
                          <td>5</td>
                          <td>100 Kg</td>
                          <td>121,000 đ</td>
                          <td>1,121,000 đ</td>
                          <td>200,000 đ</td>
                          <td>12,100,000 đ</td>
                        </tr>
                        <tr>
                          <td>
                            <tr style={{ border: "none" }}>
                              <td
                                style={{
                                  padding: 0,
                                  border: "none",
                                }}
                              >
                                <div
                                  style={{
                                    borderWidth: 1,
                                    borderColor: "#999",
                                    borderStyle: "solid",
                                    padding: 5,
                                    marginRight: 5,
                                  }}
                                >
                                  <img src={image} />
                                </div>
                              </td>
                              <td
                                style={{
                                  border: "none",
                                  textAlign: "left",
                                }}
                              >
                                Phân bón
                              </td>
                            </tr>
                          </td>
                          <td>10</td>
                          <td>5</td>
                          <td>100 Kg</td>
                          <td>121,000 đ</td>
                          <td>1,121,000 đ</td>
                          <td>200,000 đ</td>
                          <td>12,100,000 đ</td>
                        </tr>
                        <tr>
                          <td>
                            <tr style={{ border: "none" }}>
                              <td style={{ border: "none", padding: 0 }}>
                                <div
                                  style={{
                                    borderWidth: 1,
                                    borderColor: "#999",
                                    borderStyle: "solid",
                                    padding: 5,
                                    marginRight: 5,
                                  }}
                                >
                                  <img src={image} />
                                </div>
                              </td>
                              <td
                                style={{
                                  border: "none",
                                  textAlign: "left",
                                }}
                              >
                                Phân bón
                              </td>
                            </tr>
                          </td>
                          <td>10</td>
                          <td>5</td>
                          <td>100 Kg</td>
                          <td>121,000 đ</td>
                          <td>1,121,000 đ</td>
                          <td>200,000 đ</td>
                          <td>12,100,000 đ</td>
                        </tr>
                      </tbody>
                      <tfoot>
                        <tr>
                          <td
                            colSpan={8}
                            style={{
                              paddingLeft: 220,
                              paddingTop: 30,
                              paddingRight: 0,
                            }}
                          >
                            <div
                              style={{
                                display: "flex",
                                justifyContent: "space-between",
                                borderBottomStyle: "solid",
                                borderBottomWidth: 1,
                                borderBottomColor: "#999",
                                fontWeight: 700,
                                marginBottom: 5,
                                paddingBottom: 5,
                                fontStyle: "italic",
                              }}
                            >
                              <div>
                                <div>Tổng tạm tính</div>
                                <div>Tổng khuyến mãi</div>
                              </div>
                              <div
                                style={{
                                  display: "flex",
                                  textAlign: "right",
                                  marginRight: 10,
                                }}
                              >
                                <div>
                                  <div>
                                    <div>180,000,000 đ</div>
                                    <div>10,000,000 đ</div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div
                              style={{
                                display: "flex",
                                justifyContent: "space-between",
                                borderBottomStyle: "solid",
                                borderBottomWidth: 1,
                                borderBottomColor: "#999",
                                color: "#399f6b",
                                fontWeight: 700,
                                marginBottom: 5,
                                paddingBottom: 5,
                                fontStyle: "italic",
                              }}
                            >
                              <div>
                                <div>
                                  <div>Tổng thành tiền</div>
                                  <div>Thuế VAT</div>
                                </div>
                              </div>
                              <div
                                style={{
                                  display: "flex",
                                  textAlign: "right",
                                  marginRight: 10,
                                }}
                              >
                                <div>
                                  <div>
                                    <div>169,000,000 đ</div>
                                    <div>16,980,000 đ</div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div
                              style={{
                                display: "flex",
                                justifyContent: "space-between",
                                color: "#399f6b",
                                fontWeight: 700,
                                marginBottom: 5,
                                paddingBottom: 5,
                                fontStyle: "italic",
                              }}
                            >
                              <div>
                                <div>Tổng thanh toán</div>
                              </div>
                              <div
                                style={{
                                  display: "flex",
                                  textAlign: "right",
                                  marginRight: 10,
                                }}
                              >
                                <div>152,820,000 đ</div>
                              </div>
                            </div>
                          </td>
                        </tr>
                      </tfoot>
                    </table>
                    {/* <table className="table">
                      <tr style={{ backgroundColor: "#eefff6" }}>
                        <tr
                          style={{
                            paddingTop: 5,
                            paddingRight: 10,
                            paddingBottom: 5,
                            paddingLeft: 10,
                            width: '100%'
                          }}
                        >
                          <td style={{ width: 200 }}>Tên sản phẩm</td>
                          <td>Quy cách 1</td>
                          <td>Quy cách 2</td>
                          <td>Trọng lượng</td>
                          <td>Đơn giá</td>
                          <td>Thành tiên</td>
                          <td>Giảm giá</td>
                          <td>Thanh toán</td>
                        </tr>
                      </tr>
                      <tr>
                        <td style={{ padding: 0 }}>
                          <div
                            style={{
                              width: "100%",
                              height: 200,
                              overflow: "auto",
                            }}
                          >
                            <table style={{ width: "100%", border: 0 }}>
                              <tbody>
                                <tr>
                                  <td>
                                    <tr style={{ border: "none" }}>
                                      <td
                                        style={{ border: "none", padding: 0 }}
                                      >
                                        <div
                                          style={{
                                            borderWidth: 1,
                                            borderColor: "#999",
                                            borderStyle: "solid",
                                            padding: 5,
                                            marginRight: 5,
                                          }}
                                        >
                                          <img src={image} />
                                        </div>
                                      </td>
                                      <td
                                        style={{
                                          border: "none",
                                          textAlign: "left",
                                        }}
                                      >
                                        Phân bón
                                      </td>
                                    </tr>
                                  </td>
                                  <td>10</td>
                                  <td>5</td>
                                  <td>100 Kg</td>
                                  <td>121,000 đ</td>
                                  <td>1,121,000 đ</td>
                                  <td>200,000 đ</td>
                                  <td>12,100,000 đ</td>
                                </tr>
                                <tr>
                                  <td>
                                    <tr style={{ border: "none" }}>
                                      <td
                                        style={{ border: "none", padding: 0 }}
                                      >
                                        <div
                                          style={{
                                            borderWidth: 1,
                                            borderColor: "#999",
                                            borderStyle: "solid",
                                            padding: 5,
                                            marginRight: 5,
                                          }}
                                        >
                                          <img src={image} />
                                        </div>
                                      </td>
                                      <td
                                        style={{
                                          border: "none",
                                          textAlign: "left",
                                        }}
                                      >
                                        Phân bón
                                      </td>
                                    </tr>
                                  </td>
                                  <td>10</td>
                                  <td>5</td>
                                  <td>100 Kg</td>
                                  <td>121,000 đ</td>
                                  <td>1,121,000 đ</td>
                                  <td>200,000 đ</td>
                                  <td>12,100,000 đ</td>
                                </tr>
                                <tr>
                                  <td>
                                    <tr style={{ border: "none" }}>
                                      <td
                                        style={{
                                          padding: 0,
                                          border: "none",
                                        }}
                                      >
                                        <div
                                          style={{
                                            borderWidth: 1,
                                            borderColor: "#999",
                                            borderStyle: "solid",
                                            padding: 5,
                                            marginRight: 5,
                                          }}
                                        >
                                          <img src={image} />
                                        </div>
                                      </td>
                                      <td
                                        style={{
                                          border: "none",
                                          textAlign: "left",
                                        }}
                                      >
                                        Phân bón
                                      </td>
                                    </tr>
                                  </td>
                                  <td>10</td>
                                  <td>5</td>
                                  <td>100 Kg</td>
                                  <td>121,000 đ</td>
                                  <td>1,121,000 đ</td>
                                  <td>200,000 đ</td>
                                  <td>12,100,000 đ</td>
                                </tr>
                                <tr>
                                  <td>
                                    <tr style={{ border: "none" }}>
                                      <td
                                        style={{ border: "none", padding: 0 }}
                                      >
                                        <div
                                          style={{
                                            borderWidth: 1,
                                            borderColor: "#999",
                                            borderStyle: "solid",
                                            padding: 5,
                                            marginRight: 5,
                                          }}
                                        >
                                          <img src={image} />
                                        </div>
                                      </td>
                                      <td
                                        style={{
                                          border: "none",
                                          textAlign: "left",
                                        }}
                                      >
                                        Phân bón
                                      </td>
                                    </tr>
                                  </td>
                                  <td>10</td>
                                  <td>5</td>
                                  <td>100 Kg</td>
                                  <td>121,000 đ</td>
                                  <td>1,121,000 đ</td>
                                  <td>200,000 đ</td>
                                  <td>12,100,000 đ</td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </td>
                      </tr>
                      <tfoot>
                        <tr>
                          <td
                            colSpan={8}
                            style={{
                              paddingLeft: 220,
                              paddingTop: 30,
                              paddingRight: 0,
                            }}
                          >
                            <div
                              style={{
                                display: "flex",
                                justifyContent: "space-between",
                                borderBottomStyle: "solid",
                                borderBottomWidth: 1,
                                borderBottomColor: "#999",
                                fontWeight: 700,
                                marginBottom: 5,
                                paddingBottom: 5,
                                fontStyle: "italic",
                              }}
                            >
                              <div>
                                <div>Tổng tạm tính</div>
                                <div>Tổng khuyến mãi</div>
                              </div>
                              <div
                                style={{
                                  display: "flex",
                                  textAlign: "right",
                                  marginRight: 10,
                                }}
                              >
                                <div>
                                  <div>
                                    <div>180,000,000 đ</div>
                                    <div>10,000,000 đ</div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div
                              style={{
                                display: "flex",
                                justifyContent: "space-between",
                                borderBottomStyle: "solid",
                                borderBottomWidth: 1,
                                borderBottomColor: "#999",
                                color: "#399f6b",
                                fontWeight: 700,
                                marginBottom: 5,
                                paddingBottom: 5,
                                fontStyle: "italic",
                              }}
                            >
                              <div>
                                <div>
                                  <div>Tổng thành tiền</div>
                                  <div>Thuế VAT</div>
                                </div>
                              </div>
                              <div
                                style={{
                                  display: "flex",
                                  textAlign: "right",
                                  marginRight: 10,
                                }}
                              >
                                <div>
                                  <div>
                                    <div>169,000,000 đ</div>
                                    <div>16,980,000 đ</div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div
                              style={{
                                display: "flex",
                                justifyContent: "space-between",
                                color: "#399f6b",
                                fontWeight: 700,
                                marginBottom: 5,
                                paddingBottom: 5,
                                fontStyle: "italic",
                              }}
                            >
                              <div>
                                <div>Tổng thanh toán</div>
                              </div>
                              <div
                                style={{
                                  display: "flex",
                                  textAlign: "right",
                                  marginRight: 10,
                                }}
                              >
                                <div>152,820,000 đ</div>
                              </div>
                            </div>
                          </td>
                        </tr>
                      </tfoot>
                    </table> */}
                  </div>
                </Col>
                <Col xl={7} xxl={6} span={24}>
                  <div
                    className="table2"
                    style={{ marginTop: 75, width: "100%" }}
                  >
                    <table className="table">
                      <thead>
                        <th>Quà tặng kèm</th>
                        <th>
                          Số lượng
                          <div className="quy-cach">
                            <span style={{ marginRight: 1 }}>&#10088;</span>Quy
                            cách nhỏ nhất
                            <span style={{ marginLeft: 1 }}>&#10089;</span>
                          </div>
                        </th>
                      </thead>
                      <tbody>
                        <td>
                          <tr style={{ border: "none" }}>
                            <td style={{ border: "none", padding: 0 }}>
                              <div
                                style={{
                                  borderWidth: 1,
                                  borderColor: "#999",
                                  borderStyle: "solid",
                                  padding: 5,
                                  marginRight: 5,
                                }}
                              >
                                <img src={image} />
                              </div>
                            </td>
                            <td style={{ border: "none" }}>Phân bón</td>
                          </tr>
                        </td>
                        <td>10</td>
                      </tbody>
                    </table>
                  </div>
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
      </div>
    );
}

export default UI;
