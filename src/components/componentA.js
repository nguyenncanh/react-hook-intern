import React, { Component } from "react";
import ComponentB from "./componentB";

const ComponentA = (props) => {
  return (
    <div>
      <ComponentB />
    </div>
  );
};

export default ComponentA;
