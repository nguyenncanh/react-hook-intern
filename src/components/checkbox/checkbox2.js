import React, { useState } from "react";
import "./checkbox.css";

function CheckBox() {
    const [lisDiv, setListDev] = useState([
        {
            isCheck: false,
        },
        {
            isCheck: false,
        },
        {
            isCheck: false,
        },
        {
            isCheck: false,
        },
    ]);

    const changeIsCheck = (index) => {
        let shallow = [...lisDiv];

        if (!shallow[index].isCheck) {
            shallow[index].isCheck = true;
        } else {
            shallow.map((item, indexShallow) => {
                if (index !== indexShallow) {
                    item.isCheck = false
                }
            })
        }

        setListDev(shallow);
    }

    return (
        <div className="CheckBox">
            {lisDiv.map((item, index) => {
                return (
                <div
                    key={index}
                    onClick={() => changeIsCheck(index)}
                    className="box"
                    style={{ background: item.isCheck ? "blue" : "red" }}
                >
                    {index + 1}
                </div>
                );
            })}
        </div>
    );
}

export default CheckBox;
