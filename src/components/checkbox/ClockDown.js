import React, { useState, useEffect } from "react";
import "./checkbox.css";

function ClockDown() {
  const [number, setNumber] = useState(10);

  useEffect(() => {
    let id;
    if (number > 0) {
      id = setInterval(() => {
        setNumber(number - 1);
        console.log(number);
      }, 1000);
    }
    return () => {
      clearInterval(id);
    };
  }, [number]);

  const reset = () => {
    if (number === 0) {
      setNumber(10);
    }
  };

  return (
    <div>
      <div className="ClockDown">{number}</div>
      <button
        style={number !== 0 ? { cursor: "not-allowed", opacity: 0.5 } : {}}
        onClick={reset}
      >
        reset
      </button>
    </div>
  );
}

export default ClockDown;
