import React, { Component } from "react";
import "./pagination.css";

const Pagination = (props) => {
  const pagina = (number) => {
    let { pagina } = props;
    pagina(number);
  };

  const prevButton = () => {
    let { prevButton } = props;
    prevButton(true);
  };

  const nextButton = () => {
    let { nextButton } = props;
    nextButton(true);
  };

  const { postsPerPage, totalPosts, currentPage } = props;
  const pageNumbers = [];
  for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
    pageNumbers.push(i);
  }
  return (
    <nav>
      <ul className="ul-pagi">
        {currentPage === 1 ? (
          <li className="prev">
            <button
              className="prevButton"
              disabled
              style={{ cursor: "not-allowed", opacity: 0.65 }}
            >
              Previous
            </button>
          </li>
        ) : (
          <li className="prev">
            <button className="prevButton" onClick={() => prevButton()}>
              Previous
            </button>
          </li>
        )}
        {pageNumbers.map((number, index) => {
          return (
            <li className="number-pag" key={index}>
              {currentPage !== number ? (
                <button
                  className="numberButton"
                  onClick={() => pagina(number)}
                  href="!#"
                >
                  {number}
                </button>
              ) : (
                <button
                  className="numberButton"
                  onClick={() => pagina(number)}
                  href="!#"
                  style={{ borderColor: "#1890ff", color: "#1890ff" }}
                >
                  {number}
                </button>
              )}
            </li>
          );
        })}
        {currentPage === Math.ceil(totalPosts / postsPerPage) ? (
          <li className="next">
            <button
              disabled
              className="nextButton"
              style={{ cursor: "not-allowed", opacity: 0.65 }}
            >
              Next
            </button>
          </li>
        ) : (
          <li className="next">
            <button className="nextButton" onClick={() => nextButton()}>
              Next
            </button>
          </li>
        )}
      </ul>
    </nav>
  );
};

export default Pagination;
