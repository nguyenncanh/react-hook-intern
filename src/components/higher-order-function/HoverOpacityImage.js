import React, { useState } from "react";

const HoverOpacityImage = (WrappedComponent, opacity = 0.5) => {
  const [isHovered, setIsHovered] = useState(false);

  const onMouseEnter = () => {
    setIsHovered(true);
  };

  const onMouseLeave = () => {
    setIsHovered(false);
  };
  return () => {
    return (
      <div
        style={{
          opacity: isHovered ? opacity : 1,
          width: 300,
          display: "flex",
          justifyContent: "center",
        }}
        onMouseEnter={onMouseEnter}
        onMouseLeave={onMouseLeave}
      >
        <WrappedComponent />
      </div>
    );
  };
};

export default HoverOpacityImage;
