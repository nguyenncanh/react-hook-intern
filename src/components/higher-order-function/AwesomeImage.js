import React from "react";

const AwesomeImage = (props) => {
  return (
    <div>
      <img
        style={{ borderRadius: 20 }}
        alt=""
        src="https://picsum.photos/300/150"
      />
    </div>
  );
};

export default AwesomeImage;
