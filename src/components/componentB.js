import React, { Component } from "react";
import ComponentC from "./componentC.js";

const ComponentB = (props) => {
  return (
    <div>
      <ComponentC />
    </div>
  );
};

export default ComponentB;
