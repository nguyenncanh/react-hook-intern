import React from "react";

const ValueContext = React.createContext({
  changeName: (index) => {
    alert(index);
  },
});

export const ValueProvider = ValueContext.Provider;
export const ValueConsumer = ValueContext.Consumer;

export default ValueContext;
