import React, { useContext } from "react";
import ComponentD from "./componentD";

const ComponentC = (props) => {
  return (
    <div>
      <ComponentD />
    </div>
  );
};

export default ComponentC;
