import React, { Component } from "react";
import "../products/products.css";

const Products = (props) => {
    let { posts } = props;
    let image = "https://picsum.photos/300/200";
    return (
        <div>
            <h1>Posts</h1>
            <div className="posts-page">
            {posts.map((post, index) => {
                return (
                    <div className="Cart-root" key={index}>
                        <div className="Cart">
                        <img
                            className="img-title"
                            alt="somtthing"
                            src={post.image || image}
                        ></img>
                        <div className="cart-content">
                            <div className="content">{post.name}</div>
                            <div className="view-count">{post.min_unit}</div>
                            <div className="cart-time">
                            <div className="info-time">
                                <img
                                className="icon-img"
                                alt="somtthing"
                                src="https://res.cloudinary.com/daily-now/image/upload/t_logo,f_auto/v1/logos/angular"
                                ></img>
                                <div className="time">3 hour ago</div>
                            </div>
                            <span className="flaticon-bookmark"></span>
                            </div>
                        </div>
                        </div>
                    </div>
                );
            })}
            </div>
        </div>
    );
}

export default Products;
