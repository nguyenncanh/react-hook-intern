import React, { useState, useEffect } from "react";
import Products from "../products/products";
import Pagination from "../pagination/pagination";
import { configure } from "@testing-library/react";
import "antd/dist/antd.css";
import { Row, Col } from "antd";

const PostsPage = () => {
  const url = "http://61.28.235.133:22001";

  const [token, setToken] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const [posts, setPosts] = useState([]);
  const [postsPerPage, setPostsPerPage] = useState(10);
  const [totalPosts, setTotalPosts] = useState(1);

  const postLogin = async () => {
    let acc = {
      username: "tanvi",
      password: "123456",
    };
    await fetch(`${url}/admincms/login`, {
      method: "POST",
      body: JSON.stringify(acc),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setToken(data.data.token);
        // localStorage.setItem("token", data.data.token);
        console.log("token: ", data.data.token);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const getData = (number, token) => {
    fetch(
      `${url}/product/get_all?user_id=1&page=${number}&status=0&key=&category_id=0&type=0`,
      {
        method: "GET",
        headers: {
          "Content-type": "application/json; charset=UTF-8",
          token: token,
        },
      }
    )
      .then((res) => res.json())
      .then((res) => {
        setPosts(res.data.product);
        setTotalPosts(res.data.total);
        setPostsPerPage(res.data.size);
        console.log("data: ", res);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    postLogin();
  }, []);

  useEffect(() => {
    if (token) {
      getData(currentPage, token);
    }
  }, [currentPage, token]);

  const handlePagination = (number) => {
    if (number !== currentPage) {
      setCurrentPage(number);
    }
  };

  const prevButton = (status) => {
    if (status) {
      if (currentPage > 1) {
        setCurrentPage(currentPage - 1);
      }
    }
  };

  const nextButton = (status) => {
    if (status) {
      if (currentPage < Math.ceil(totalPosts / postsPerPage)) {
        setCurrentPage(currentPage + 1);
      }
    }
  };
  const style = {
    background: "red",
    color: "#fff",
    borderWith: 1,
    borderColor: "#000",
    borderStyle: "solid",
    borderRadius: 4,
  };
  return (
    <div>
      <Products posts={posts} />
      <Pagination
        postsPerPage={postsPerPage}
        totalPosts={totalPosts}
        currentPage={currentPage}
        pagina={handlePagination}
        prevButton={prevButton}
        nextButton={nextButton}
      />
      {/* <Row gutter={[16, 16]}>
                        <Col style={style} xs={24} sm={24} md={24} lg={12} xl={12} xxl={8}>
                            Col
                        </Col>
                        <Col style={style} xs={24} sm={24} md={24} lg={12} xl={12} xxl={8}>
                            Col
                        </Col>
                        <Col style={style} xs={24} sm={24} md={24} lg={12} xl={12} xxl={8}>
                            Col
                        </Col>
                    </Row> */}
    </div>
  );
};

export default PostsPage;
