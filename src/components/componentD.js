import React, { Component, useContext } from "react";
import ValueContext from "../components/valueContext";

const ComponentD = (props) => {
  return (
    <div>
      <h1>This is componentD</h1>
      <ValueContext.Consumer>
        {({ users, changeName }) => (
          <div>
            {users.map((user, index) => {
              return (
                <div key={index}>
                  <h3>My name is {user.name}</h3>
                  <p>And I'm {user.age} years old</p>
                  <button
                    onClick={() => {
                      changeName(index);
                    }}
                  >
                    Click me
                  </button>
                </div>
              );
            })}
          </div>
        )}
      </ValueContext.Consumer>
    </div>
  );
};

export default ComponentD;
