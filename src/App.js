import React, { useState, useEffect } from "react";
import "./App.css";
import CheckBox from "./components/checkbox/checkbox";
import CheckBox2 from "./components/checkbox/checkbox2";
import PostsPage from "./components/products/productsPage";
import UI from "./components/ui-loctroi/ui";
import ClockDown from "./components/checkbox/ClockDown";
import valueContext from "./components/valueContext";
import ComponentA from "./components/componentA.js";
import AwesomeImage from "./components/higher-order-function/AwesomeImage";
import HoverOpacityImage from "./components/higher-order-function/HoverOpacityImage";

function App() {
  const users = [
    {
      name: "canh",
      age: 22,
    },
    {
      name: "dasda",
      age: 22,
    },
    {
      name: "sadas",
      age: 22,
    },
    {
      name: "eqwe",
      age: 22,
    },
    {
      name: "hgfhf",
      age: 22,
    },
  ];

  const changeName = (index) => {
    users.map((user, indexUser) => {
      if (indexUser === index) {
        user.name = "changed";
      }
      return user;
    });
    setValue({ ...value, users });
  };

  const [value, setValue] = useState({
    users: users,
    changeName: changeName,
  });

  const initState = {
    "input-1": "",
    "input-2": "",
    "input-3": "",
    "input-4": "",
  };

  const [stringInput, setStringInput] = useState("");

  const [number, setNumber] = useState();

  // const onChange = (e) => {
  //   const { value, id } = e.target;
  //   setForm({ ...form, [id]: value });
  //   if (value.length === 1) {
  //     setTotal((str) => str + value);
  //     const ids = id.split("-");
  //     console.log("ids: ", ids);
  //     const num = Number(ids[1]);
  //     console.log("num: ", num);
  //     const nextInput = document.getElementById(`${ids[0]}-${num + 1}`);
  //     console.log("nextInput: ", nextInput);
  //     if (nextInput !== null) {
  //       nextInput.focus();
  //     }
  //     if (nextInput === null) {
  //       setNumber(10);
  //       setTimeout(() => {
  //         setForm(initState);
  //         setTotal("");
  //         document.getElementById("input-1").focus();
  //       }, 10000);
  //     }
  //   }
  // };

  const onFocus = (e) => {
    let textInput = document.getElementById("text-input");
    textInput.focus();
  };

  const onChangeTextIpnut = (e) => {
    setStringInput(e.target.value);
  };

  useEffect(() => {
    let id;
    if (number > 0) {
      id = setInterval(() => {
        setNumber(number - 1);
        console.log(number);
      }, 1000);
    }
    return () => {
      clearInterval(id);
    };
  }, [number]);

  // higher-order-function:
  const HoverOpacityAwesomeImage = HoverOpacityImage(AwesomeImage, 0.4);

  return (
    // <valueContext.Provider value={value}>
    //   <div className="App">
    //     {/* <CheckBox />
    //   <CheckBox2 /> */}
    //     {/* <PostsPage /> */}
    //     {/* <UI /> */}
    //     {/* <ClockDown /> */}
    //     <ComponentA />
    //   </div>
    // </valueContext.Provider>

    // <div>
    //   <div style={{ display: "flex", justifyContent: "center" }}>
    //     <input
    //       type="text"
    //       maxLength={1}
    //       style={{ width: 50, height: 50, textAlign: "center" }}
    //       onFocus={onFocus}
    //       id="input-1"
    //       defaultValue={stringInput[0]}
    //     />
    //     <input
    //       type="text"
    //       maxLength={1}
    //       style={{
    //         width: 50,
    //         height: 50,
    //         margin: "0 10px 0 10px",
    //         textAlign: "center",
    //       }}
    //       onFocus={onFocus}
    //       id="input"
    //       defaultValue={stringInput[1]}
    //     />
    //     <input
    //       type="text"
    //       maxLength={1}
    //       style={{
    //         width: 50,
    //         height: 50,
    //         margin: "0 10px 0 0",
    //         textAlign: "center",
    //       }}
    //       onFocus={onFocus}
    //       id="input"
    //       defaultValue={stringInput[2]}
    //     />
    //     <input
    //       type="text"
    //       maxLength={1}
    //       style={{ width: 50, height: 50, textAlign: "center" }}
    //       onFocus={onFocus}
    //       id="input"
    //       defaultValue={stringInput[3]}
    //     />
    //   </div>
    //   <div
    //     style={{
    //       display: "flex",
    //       justifyContent: "center",
    //       marginTop: 20,
    //       opacity: 0,
    //     }}
    //   >
    //     <input
    //       id="text-input"
    //       type="text"
    //       maxLength={4}
    //       onChange={onChangeTextIpnut}
    //     />
    //   </div>
    // </div>

    // higher-order-function:
    // <div>
    //   <HoverOpacityAwesomeImage />
    // </div>

    <div>
      <a
        href="https://res.cloudinary.com/nguyencanh/image/upload/v1589285129/g3u0w4hhdmekd3gdlu74.jpg"
        download
      >
        export excel using tag a with attribute download
      </a>
    </div>
  );
}

export default App;
